# CharWordCount

A simple character and word count checker built in Qt.

## Building

Built in Qt Creator with Qt 6.7.3 and compiled with GCC 14.2.1. Other versions of Qt may also work.

You can build by running `qmake6` followed by `make`.

## License
[![AGPL](https://www.gnu.org/graphics/agplv3-155x51.png)](https://www.gnu.org/licenses/agpl-3.0.en.html)

This is free software under the terms of the GNU Affero General Public License v3.0. You are free to use, study, share, and modify it for any purpose.

<a href="https://endsoftwarepatents.org/innovating-without-patents"><img src="https://static.fsf.org/nosvn/esp/logos/patent-free.svg" width="120" height="120"></a>
