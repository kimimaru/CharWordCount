#include "charwordgui.h"

#include <QApplication>
#include <QStyleFactory>

int main(int argc, char *argv[])
{
    //Use system theme
    QApplication::setDesktopSettingsAware(true);

    QApplication a(argc, argv);
    CharWordGUI w;
    w.show();
    return a.exec();
}
