#include "charwordgui.h"
#include "ui_charwordgui.h"
#include <iostream>

CharWordGUI::CharWordGUI(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::CharWordGUI)
{
    ui->setupUi(this);

    // Link the text box's textChanged signal with our OnTextChanged receiver
    connect(ui->textBox, &QTextEdit::textChanged, this, &CharWordGUI::OnTextChanged);

    whiteSpaceRegex = QRegularExpression("\\s+", QRegularExpression::CaseInsensitiveOption);
    newLineRegex = QRegularExpression("[\r\n]", QRegularExpression::CaseInsensitiveOption);

    OnTextChanged();
}

void CharWordGUI::OnTextChanged()
{
    UpdateCharCount();
    UpdateWordCount();
    UpdateLineCount();
}

int CharWordGUI::GetCharCount()
{
    // Simply return how many characters are in the text box
    QString text = ui->textBox->toPlainText();
    return text.length();
}

int CharWordGUI::GetWordCount()
{
    // Get the plain text as a string
    QString text = ui->textBox->toPlainText();

    // Match all whitespace in a regex expression and split by that value
    QStringList textList = text.split(whiteSpaceRegex, Qt::SkipEmptyParts);

    // Return the number of entries
    return textList.length();
}

int CharWordGUI::GetLineCount()
{
    QString text = ui->textBox->toPlainText();

    // Match all new lines in a regex expression and split by that value
    QStringList textList = text.split(newLineRegex, Qt::KeepEmptyParts);

    // Return the number of entries
    return textList.length();
}

void CharWordGUI::UpdateCharCount()
{
    int charCount = GetCharCount();

    // Update character count text
    ui->charCountOutput->setText(QString::number(charCount));
}

void CharWordGUI::UpdateWordCount()
{
    int wordCount = GetWordCount();

    // Update word count text
    ui->wordCountOutput->setText(QString::number(wordCount));
}

void CharWordGUI::UpdateLineCount()
{
    int lineCount = GetLineCount();

    ui->lineCountOutput->setText(QString::number(lineCount));
}

CharWordGUI::~CharWordGUI()
{
    delete ui;
}
