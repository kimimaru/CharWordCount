#ifndef CHARWORDGUI_H
#define CHARWORDGUI_H

#include <QMainWindow>
#include <QRegularExpression>
#include <QTextEdit>

QT_BEGIN_NAMESPACE
namespace Ui {
class CharWordGUI;
}
QT_END_NAMESPACE

class CharWordGUI : public QMainWindow {
    Q_OBJECT

public:
    CharWordGUI(QWidget* parent = nullptr);
    ~CharWordGUI();
    int GetCharCount();
    int GetWordCount();
    int GetLineCount();
    void UpdateCharCount();
    void UpdateWordCount();
    void UpdateLineCount();

private:
    QRegularExpression whiteSpaceRegex;
    QRegularExpression newLineRegex;

    Ui::CharWordGUI* ui;
    void OnTextChanged();
};
#endif // CHARWORDGUI_H
